# Qwick and Dirty Migration System

Qwick and Dirty Migration System (qdms) is used for iterative Postgres DB development.

Most modern DB migration systems work on a single migration script for each change. For changing things that hold data (i.e. tables) this works really well and has worked pretty well for a long time.

If using a development style that is based on iterative development though, this migration system does not scale well. his lead to the rise of NOSQL type DBs where the schema itself was considered the Each change is a new migration which grows quickly, migrations work off of a timestamp which is difficult to use in a team of de This lead to the rise of NOSQL type DBs where the schema itself was considered the enemy. The schema itself is not the enemy, the difficulty of changing it through modern migration systems is.velopers, and things like functions/views are very difficult to diff in modern VCS.

qdms looks to change this through some different design decisions:

1. Your DB migration shouldn't need to be timestamped based. Migrations should form a dependency tree and be applied in that order.
1. Schema changes that do not affect any underlying user data (e.g. changing a view or a function) should be as simple as changing a SQL file. These should be able to be rolled-back and then re-applied at will.
1. Schema changes that do affect underlying user should have a save/restore script that will allow them to be safely moved out of the way while underlying schema requirements are being modified. After the modifications are done, they should be restored with user data intact.
1. Changing an underlying requirement should trigger a save/restore of all requirements that will be automatically applied.
1. Safe-by-default. Migrations are run in a "dry-run" state by default, showing the user what will be save/restored/run without running anything. Migrations that affect data require a save/restore script. All schema changes are run inside of 1 transaction.
1. Previously run deploys should have their save/restore/revert scripts saved in the DB so you revert a change without that change existing anymore
