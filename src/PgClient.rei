type pgClient;

module Result: {
  type t;
  let rowsGet: t => array(Js.Json.t);
  let make: (~rows: array(Js.Json.t)) => t;
};
module QueryPayload: {
  type t;
  let textGet: t => string;
  let valuesGet: t => option(array(Js.Nullable.t(string)));
  let make:
    (~text: string, ~values: array(Js.Nullable.t(string))=?, unit) => t;
};
let make: string => Future.t(Belt.Result.t(pgClient, string));
let query:
  (~values: array(Js.Nullable.t(string))=?, ~query: string, pgClient) =>
  Future.t(Belt.Result.t(array(Js.Json.t), string));
