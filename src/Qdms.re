type args = {databaseUrl: string};
type migration = {
  migrationName: string,
  migration: string,
};

let getArgs = () => {
  let databaseUrl =
    ref(
      Node.Process.process##env->Js.Dict.get("DATABASE_URL")
      |> Relude.Option.getOrElseStrict(""),
    );

  let args = [
    (
      "-database-url",
      Arg.Set_string(databaseUrl),
      "The full connection string to the database",
    ),
  ];

  Arg.parse(args, _ => (), "");

  switch ({databaseUrl: databaseUrl^}) {
  | {databaseUrl: ""} => Js.Exn.raiseError("Missing Database Url")
  | args => args
  };
};

let getSqlMigrationFiles = path =>
  (
    switch (
      Relude.(
        path
        |> Node.Fs.readdirSync
        |> List.fromArray
        |> List.filter(String.endsWith(".sql"))
        |> List.sort((module String.Ord))
        |> List.map(migrationName =>
             {
               migrationName,
               migration: migrationName |> Node.Fs.readFileAsUtf8Sync,
             }
           )
      )
    ) {
    | exception e =>
      e
      |> Relude.Js.Exn.fromExn
      |> Js.Exn.message
      |> Relude.Option.getOrElseStrict(
           "An unknown error occurred while opening the SQL migration files",
         )
      |> Relude.Result.error
    | paths => paths |> Relude.Result.ok
    }
  )
  |> Future.value;

let performMigrationIfNeeded = (pgClient, {migrationName, migration}) =>
  PgClient.query(
    ~query="SELECT * FROM qdms.migrations WHERE migration_name = $1",
    ~values=[|migrationName |> Js.Nullable.return|],
    pgClient,
  )
  ->Future.flatMapOk(results =>
      switch (results |> Relude.Array.length) {
      | 0 => PgClient.query(~query=migration, pgClient)->Future.mapOk(_ => ())
      | _ => () |> Relude.Result.ok |> Future.value
      }
    );

let rec performMigrations = (pgClient, migrations) =>
  switch (migrations) {
  | [migration, ...migrations] =>
    pgClient
    ->performMigrationIfNeeded(migration)
    ->Future.flatMapOk(_ => performMigrations(pgClient, migrations))
  | [] => () |> Relude.Result.ok |> Future.value
  };

let performMigrations = (pgClient, migrationsPath) =>
  migrationsPath
  ->getSqlMigrationFiles
  ->Future.flatMapOk(performMigrations(pgClient));

  let runRevertTransientScript = (pgClient, path) => 
