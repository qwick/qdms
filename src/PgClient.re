let (>>) = Relude.Function.Infix.(>>);

type pgClient;

module Result = {
  [@bs.deriving abstract]
  type t = {rows: array(Js.Json.t)};

  let make = t;
};

module QueryPayload = {
  [@bs.deriving abstract]
  type t = {
    text: string,
    [@bs.optional]
    values: array(Js.Nullable.t(string)),
  };

  let make = t;
};

[@bs.module "pg"] [@bs.new]
external make: {. "connectionString": string} => pgClient = "Client";
let make = connectionString => make({"connectionString": connectionString});

[@bs.send] external connect: pgClient => Js.Promise.t(unit) = "";

let handlePromiseFailure =
  [@bs.open]
  (
    fun
    | Js.Exn.Error(e) => e |> Js.Exn.message
  );

let handlePromiseFailure = promiseFailure =>
  promiseFailure
  |> handlePromiseFailure
  |> Relude.Option.flatMap(Relude.Function.identity)
  |> Relude.Option.getOrElseStrict("An unknown error has occurred");

let connect = pgClient =>
  pgClient
  ->connect
  ->FutureJs.fromPromise(handlePromiseFailure)
  ->Future.mapOk(_ => pgClient);

let make = make >> connect;

[@bs.send]
external query: (pgClient, QueryPayload.t) => Js.Promise.t(Result.t) = "";
let query = (~values=?, ~query as text, t) =>
  t->query(QueryPayload.make(~text, ~values?, ()));
let query = (~values=?, ~query as text, t) =>
  query(~values?, ~query=text, t)
  ->FutureJs.fromPromise(_ => "Query: '" ++ text ++ "' failed")
  ->Future.mapOk(Result.rowsGet);
